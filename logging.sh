#!/usr/bin/env bash
set -e

LOG_LEVEL='INFO'
NORMAL='\e[0m'

logger(){
    LOGGING="$1"
    MESSAGE="$2"
    TIMESTAMP="[$(date)]"

    case "$LOG_LEVEL" in
        DEBUG)
            DEBUG=true
            INFO=true
            WARN=true
            ERROR=true
            ;;
        INFO)
            DEBUG=false
            INFO=true
            WARN=true
            ERROR=true
            ;;
        WARN)
            DEBUG=false
            INFO=false
            WARN=true
            ERROR=true
            ;;
        ERROR)
            DEBUG=false
            INFO=false
            WARN=false
            ERROR=true
            ;;
        *)
            echo "Invalid LOG_LEVEL - $LOG_LEVEL"
            exit 2
            ;;
    esac

    case "$LOGGING" in
        DEBUG)
            COLOR='\e[35m'
            "$DEBUG" || return 0
            EXIT_CODE=0
            ;;
        INFO)
            COLOR='\e[36m'
            "$INFO" || return 0
            EXIT_CODE=0
            ;;
        WARN)
            COLOR='\e[33m'
            "$WARN" || return 0
            EXIT_CODE=0
            ;;
        ERROR)
            COLOR='\e[31m'
            "$ERROR" || return 0
            EXIT_CODE=2
            ;;
        *)
            echo "Invalid LOG_LEVEL - $LOGGING"
            EXIT_CODE=3
            ;;
    esac

    echo -e "$TIMESTAMP - ${COLOR}${LOGGING}${NORMAL} - $MESSAGE"

    if [ "$EXIT_CODE" != "0" ]; then
        exit "$EXIT_CODE";
    fi
}
