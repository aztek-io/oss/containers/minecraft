#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
set -e

BASE_PATH="$(dirname "$0")"

source "${BASE_PATH}/logging.sh"

OPS_PATH="${HOME}/ops.txt"

check_ops_var(){
    if test -n "$MINECRAFT_OPS"; then
        logger "INFO" "MINECRAFT_OPS variable set."
        logger "INFO" "Configuring admins."
        check_new_admin_list
        check_old_admin_list
    else
        logger "WARN" "Admins are not specified.  This can be configured in the MINECRAFT_OPS environment variable."
    fi
}

check_new_admin_list(){
    while read -r ADMIN; do
        grep "$ADMIN" "$OPS_PATH" || {
            logger "INFO" "Adding $ADMIN"
            echo "$ADMIN" >> "$OPS_PATH"
        }
    done < <(
        echo "$MINECRAFT_OPS" | tr ',' '\n'
    )
}

check_old_admin_list(){
    while read -r LINE; do
        grep "$LINE" <(echo "$MINECRAFT_OPS") || {
            logger "WARN" "$LINE is configured as admin, but not in MINECRAFT_OPS environment variable."
            logger "WARN" "You will have to manually remove $LINE via the CLI if that is your intention."
        }
    done < "$OPS_PATH"
}

ops_eval(){
    check_ops_var
}
