#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
set -e

BASE_PATH="$(dirname "$0")"

source "${BASE_PATH}/logging.sh"
source "${BASE_PATH}/eula.sh"
source "${BASE_PATH}/get_minecraft.sh"
source "${BASE_PATH}/ops.sh"
source "${BASE_PATH}/run.sh"

main(){
    logger "INFO" "Starting container."
    eula_eval
    version_eval
    ops_eval
    run_minecraft
}

main
