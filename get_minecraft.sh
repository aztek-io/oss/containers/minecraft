#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
set -e

BASE_PATH="$(dirname "$0")"

source "${BASE_PATH}/logging.sh"

MANIFEST="$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json)"

check_version_var(){
    if test -z "$MINECRAFT_VERSION"; then
        logger "WARN" "Minecraft version not pinned."
        logger "INFO" "This can be set via the MINECRAFT_VERSION environment variable."
        logger "INFO" "Getting latest minecraft release."

        MINECRAFT_VERSION="$(echo "$MANIFEST" | jq -r .latest.release)"
    fi

    logger "INFO" "Downloading $MINECRAFT_VERSION."
}

download_server_jar(){
    VERSION_MANIFEST_LINK="$(echo "$MANIFEST" | jq -r --arg version "$MINECRAFT_VERSION" '.versions[] | select(.id == $version).url' )"
    logger "INFO" "VERSION_MANIFEST_LINK: ${VERSION_MANIFEST}"

    DOWNLOAD_LINK="$(curl -s "$VERSION_MANIFEST_LINK" | jq -r .downloads.server.url)"

    if [[ "$DOWNLOAD_LINK" == "null" ]]; then
        logger "ERROR" "Download link no longer available for ${MINECRAFT_VERSION}."
        exit 255
    else
        logger "INFO" "DOWNLOAD_LINK: ${DOWNLOAD_LINK}"
    fi

    curl -so "${BASE_DIR}/minecraft_server.${MINECRAFT_VERSION}.jar" "${DOWNLOAD_LINK}"

    logger "INFO" "Downloaded server jar."
}

version_eval(){
    check_version_var
    download_server_jar
}

# version_eval
