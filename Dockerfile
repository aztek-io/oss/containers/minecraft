FROM openjdk:16-jdk-alpine3.12

ARG HOME=/data
ENV HOME=$HOME

WORKDIR /scripts

COPY ["*.sh", "./"]

# hadolint ignore=DL3018
RUN apk add --no-cache \
        curl \
        vim \
        bash \
        jq

WORKDIR $HOME
ENTRYPOINT ["/scripts/entrypoint.sh"]
