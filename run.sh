#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
set -e

BASE_PATH="$(dirname "$0")"

source "${BASE_PATH}/logging.sh"

run_minecraft(){
    logger "INFO" "Finished pre-startup.  Starting Minecraft Server."

    java \
        -Xmx"${MINECRAFT_MAX_MEM_POOL:-1024M}" \
        -Xms"${MINECRAFT_INIT_MEM_POOL:-1024M}" \
        -jar "${BASE_DIR}/minecraft_server.${MINECRAFT_VERSION}.jar" \
        nogui
}
