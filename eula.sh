#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
set -e

BASE_PATH="$(dirname "$0")"

source "${BASE_PATH}/logging.sh"

EULA_PATH="${HOME}/eula.txt"

check_eula_var(){
    if [[ "$MINECRAFT_EULA" != "true" ]]; then
        logger "ERROR" "EULA not accepted.  Accept it by setting environment variable MINECRAFT_EULA=true"
    fi
}

check_eula_file(){
    if ! test -f "$EULA_PATH"; then
        logger "INFO" "Creating $EULA_PATH"
        echo "eula=${MINECRAFT_EULA}" > "$EULA_PATH"
    else
        logger "INFO" "${EULA_PATH} already exits: $(cat "${EULA_PATH}")"
    fi
}

eula_eval(){
    check_eula_var
    check_eula_file
}
