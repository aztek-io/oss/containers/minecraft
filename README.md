# Minecraft

## Running this on Unix like systems (in a bash like shell):

``` bash
docker run -it \
    -e MINECRAFT_EULA=true \
    -v "$(pwd):/data" \
    -p 25565:25565 \
    aztek/minecraft:v0.2.0
```

## Running this on Windows systems (in powershell):

``` Powershell
docker.exe run -it `
    -e MINECRAFT_EULA=true `
    -v "$(Get-Location):/data" `
    -p 25565:25565 `
    aztek/minecraft:v0.2.0
```

# Environment variables

Read more about setting environment variables on [docker.com](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file).

## Required

- `MINECRAFT_EULA` - (bool) Accept End User License Agreement

## Optional

- `MINECRAFT_VERSION`       - (string) A version number like `1.15` or `1.16.4` (defaults to the latest available release)
- `MINECRAFT_MAX_MEM_POOL`  - (string) max mem pool size (defaults to `1024M`)
- `MINECRAFT_INIT_MEM_POOL` - (string) initial memory pool size (defaults to `1024M`)
